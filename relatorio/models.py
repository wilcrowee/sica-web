# -*- coding: utf-8 -*-

from django.db import models
from aluno.models import Aluno

class Ticket(models.Model):
    aluno = models.ForeignKey(Aluno)
    data = models.DateTimeField(u"Data de emissão do Ticket")
    tipo = models.IntegerField(u"Tipo de Ticket")

class Search(models.Model):
    campo = models.CharField(max_length=255)
    
class Refeicao(models.Model):
    aluno = models.ForeignKey(Aluno)
    data = models.DateTimeField(u"Data da refeição");