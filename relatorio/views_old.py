# -*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required, permission_required
from django.http import HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from models import Ticket
from curso.models import Curso
from calendar import monthrange

from reportlab.lib.enums import TA_CENTER, TA_JUSTIFY
from reportlab.lib.styles import ParagraphStyle, getSampleStyleSheet
from reportlab.platypus import (Paragraph, SimpleDocTemplate, Spacer, Table, TableStyle)
from reportlab.lib.units import inch
from reportlab.lib import colors

import datetime, tempfile
from aluno.models import Aluno
from relatorio.models import Refeicao

@login_required(login_url='/sicaweb/accounts/login/')
def relatorios(request):
    data = datetime.datetime.now()
    itensTicket = Ticket.objects.raw("select * from relatorio_ticket where data = to_date('"+data.strftime("%Y-%m-%d")+"', 'yyyy-mm-dd')")
    itensRefeicao = Refeicao.objects.raw("select * from relatorio_refeicao where data = to_date('"+data.strftime("%Y-%m-%d")+"','yyyy-mm-dd')")
    itensTicket = len(list(itensTicket))
    itensRefeicao = len(list(itensRefeicao))
    
    if request.method == 'POST':
        if request.POST.get("tipo") == '1':
            data = request.POST.get('data')

	    if int(curso) != 0 and int(ano) != 0:
                lstAlunos = Refeicao.objects.raw("SELECT A.id, data, aluno_id FROM relatorio_refeicao R INNER JOIN aluno_aluno A on  A.id = R.aluno_id where data = to_date('"+data+"', 'dd-mm-yyyy') and curso like '"+curso+"' and ano = "+ano)
                lstTickets = Ticket.objects.raw("SELECT A.id, data, aluno_id FROM relatorio_ticket R INNER JOIN aluno_aluno A on  A.id = R.aluno_id where data = to_date('"+data+"', 'dd-mm-yyyy') and curso like '"+curso+"' and ano = "+ano)
            elif int(curso) != 0:
                lstAlunos = Refeicao.objects.raw("SELECT A.id, data, aluno_id FROM relatorio_refeicao R INNER JOIN aluno_aluno A on  A.id = R.aluno_id where data = to_date('"+data+"', 'dd-mm-yyyy') and curso like '"+curso+"'")
                lstTickets = Ticket.objects.raw("SELECT A.id, data, aluno_id FROM relatorio_ticket R INNER JOIN aluno_aluno A on  A.id = R.aluno_id where data = to_date('"+data+"', 'dd-mm-yyyy') and curso like '"+curso+"'")
            elif int(ano) != 0:
                lstAlunos = Refeicao.objects.raw("SELECT A.id, data, aluno_id FROM relatorio_refeicao R INNER JOIN aluno_aluno A on  A.id = R.aluno_id where data = to_date('"+data+"', 'dd-mm-yyyy') and ano = "+ano)
                lstTickets = Ticket.objects.raw("SELECT A.id, data, aluno_id FROM relatorio_ticket R INNER JOIN aluno_aluno A on  A.id = R.aluno_id where data = to_date('"+data+"', 'dd-mm-yyyy') and ano = "+ano)
            else:
                lstAlunos = Refeicao.objects.raw("select * from relatorio_refeicao where data = to_date('"+data+"', 'dd-mm-yyyy')")
                lstTickets = Ticket.objects.raw("select * from relatorio_ticket where data = to_date('"+data+"', 'dd-mm-yyyy')")
            
	    lstAlunos = list(lstAlunos)
            lstTickets = list(lstTickets)

            lstAlunos = lstConverter(lstTickets, lstAlunos)
            lstTickets = comparaLista(lstTickets, lstAlunos)
            
            lstAlunos.sort(key=lambda x: x.aluno.nome, reverse=False)
            lstTickets.sort(key=lambda x: x.aluno.nome, reverse=False)
                                                    
            return render(request, 'relatorio/relatorios.html', {'contadorRef': itensRefeicao, 'contador': itensTicket,'data': data,'contadorRefeicao': len(lstAlunos), 'contadorTicket': len(lstTickets), 'alunos': list(lstAlunos), 'tickets': list(lstTickets), 'id': 1, 'cursos': cursos, 'pdf_curso': curso, 'pdf_ano': ano})
        elif request.POST.get("tipo") == '2':
            data = request.POST.get('data')
            lstDay = []
            lstCount = []
            lstRef = []
            total = 0
            totalRef = 0
                 
            aux = data.split("/")    
            data_ano = int(aux[1])
            mes = int(aux[0])
            qtdDias = monthrange(data_ano, mes)[1]            
            
            for x in range(1,qtdDias+1):
                stData = str(x)+"-"+str(mes)+"-"+str(data_ano)
                weekDay = datetime.datetime(data_ano, mes, x).weekday()
                
                
                if weekDay != 5 and weekDay != 6:
		    if int(curso) != 0 and int(ano) != 0:
                        lstRefeicao = Refeicao.objects.raw("SELECT A.id, data, aluno_id FROM relatorio_refeicao R INNER JOIN aluno_aluno A on  A.id = R.aluno_id where data = to_date('"+stData+"', 'dd-mm-yyyy') and curso like '"+curso+"' and ano = "+ano)
                        lstAlunos = Ticket.objects.raw("SELECT A.id, data, aluno_id FROM relatorio_ticket R INNER JOIN aluno_aluno A on  A.id = R.aluno_id where data = to_date('"+stData+"', 'dd-mm-yyyy') and curso like '"+curso+"' and ano = "+ano)
                    elif int(curso) != 0:
                        lstRefeicao = Refeicao.objects.raw("SELECT A.id, data, aluno_id FROM relatorio_refeicao R INNER JOIN aluno_aluno A on  A.id = R.aluno_id where data = to_date('"+stData+"', 'dd-mm-yyyy') and curso like '"+curso+"'")
                        lstAlunos = Ticket.objects.raw("SELECT A.id, data, aluno_id FROM relatorio_ticket R INNER JOIN aluno_aluno A on  A.id = R.aluno_id where data = to_date('"+stData+"', 'dd-mm-yyyy') and curso like '"+curso+"'")
                    elif int(ano) != 0:
                        lstRefeicao = Refeicao.objects.raw("SELECT A.id, data, aluno_id FROM relatorio_refeicao R INNER JOIN aluno_aluno A on  A.id = R.aluno_id where data = to_date('"+stData+"', 'dd-mm-yyyy') and ano = "+ano)
                        lstAlunos = Ticket.objects.raw("SELECT A.id, data, aluno_id FROM relatorio_ticket R INNER JOIN aluno_aluno A on  A.id = R.aluno_id where data = to_date('"+stData+"', 'dd-mm-yyyy') and ano = "+ano)
                    else:
                        lstRefeicao = Refeicao.objects.raw("select * from relatorio_refeicao where data = to_date('"+stData+"', 'dd-mm-yyyy')")
                        lstAlunos = Ticket.objects.raw("select * from relatorio_ticket where data = to_date('"+stData+"', 'dd-mm-yyyy')")

		    lstRefeicao = list(lstRefeicao)
                    lstAlunos = list(lstAlunos)
		    lstDay.append(stData)
                    lstCount.append(lstAlunos.__len__())
                    total = total+(lstAlunos.__len__())
                    lstRef.append(len(lstConverter(lstAlunos, lstRefeicao)))
                    totalRef = totalRef+(lstRefeicao.__len__())
                    
                lstTable = zip(lstDay,lstCount, lstRef)
                                
            return render(request, 'relatorio/relatorios.html', {'contadorRef': itensRefeicao, 'contador': itensTicket, 'id': 2, 'cursos': cursos, 'lstTable': lstTable, 'mes': mes, 'ano': data_ano, 'total': total, 'pdf_curso': curso, 'pdf_ano': ano})
    else:
        if '0' in request.session:
            msg = request.session['0']
            del request.session['0']
            
            return render(request, 'relatorio/relatorios.html', {'contadorRef': itensRefeicao, 'contador': itensTicket,'id': 1, 'cursos': cursos, 'msg': msg})
        
        return render(request, 'relatorio/relatorios.html', {'contadorRef': itensRefeicao, 'contador': itensTicket,'id': 1, 'cursos': cursos})

@login_required(login_url='/sicaweb/accounts/login/')
@permission_required('aluno.add_aluno', raise_exception=True)
def consomeTicket(request):
    if request.method == 'POST':
        dia = request.POST.get('data')
        enviar = request.POST.get('button')
            
        lstAlunos = Refeicao.objects.raw("select * from relatorio_refeicao where data = to_date('"+dia+"', 'dd-mm-yyyy')")
        lstAlunos = list(lstAlunos)
        lstTickets = Ticket.objects.raw("select * from relatorio_ticket where data = to_date('"+dia+"', 'dd-mm-yyyy')")
        lstTickets = list(lstTickets)
    
        lstAlunos = comparaLista(lstTickets, lstAlunos)
        lstAlunos.sort(key=lambda x: x.aluno.nome, reverse=False)
                
        if enviar:
            lista = request.POST.getlist('lista')
            
            if len(lista) != 0:
                dia = datetime.datetime.strptime(dia, '%d/%m/%Y')
                
                for item in lista:
                    aluno = get_object_or_404(Aluno, pk = item)
                    
                    ref = Refeicao(aluno = aluno,
                                   data = dia)
                    
                    ref.save()
                    
                request.session[0] = "Tickets consumidos com sucesso"
                
                return redirect('/sicaweb/relatorio')
            else:
                return render(request, 'relatorio/refeicao.html', {'tickets': lstAlunos, 'dia': dia, 'err': "Nenhum registro selecionado"})
        
        return render(request, 'relatorio/refeicao.html', {'tickets': lstAlunos, 'dia': dia})
    else:
        return redirect('/sicaweb/relatorio')
    
def comparaLista(tickets, refeicoes):
    aux = []
       
    for tic in tickets:
        count = 0
        
        for ref in refeicoes:
            if tic.aluno.prontuario == ref.aluno.prontuario:
                count = count+1
        
        if count == 0:
            aux.append(tic)
    
    return aux

def lstConverter(tickets, refeicoes):
    aux = []
    
    for tic in tickets:
        count = 0
        for ref in refeicoes:
            if tic.aluno.prontuario == ref.aluno.prontuario:
                count = count+1
                if(count == 1):
                    aux.append(tic)
    
    return aux
        
def dayRefPDF(request):
    dia = request.POST.get('data')
    curso = request.POST.get('pdf_curso')
    ano = request.POST.get('pdf_ano')

    if curso != u"" and  ano != u"" and curso != u"0" and  ano != u"0":
        if curso == u"1":
            stCurso = "T&eacute;cnico Integrado em Inform&aacute;tica"
        else:
            stCurso = "T&eacute;cnico Integrado em Mecatr&ocirc;nica"
        
        lstAlunos = Refeicao.objects.raw("SELECT A.id, data, aluno_id FROM relatorio_refeicao R INNER JOIN aluno_aluno A on  A.id = R.aluno_id where data = to_date('"+dia+"', 'dd-mm-yyyy') and curso like '"+curso+"' and ano = "+ano)
        lstTickets = Ticket.objects.raw("SELECT A.id, data, aluno_id FROM relatorio_ticket R INNER JOIN aluno_aluno A on  A.id = R.aluno_id where data = to_date('"+dia+"', 'dd-mm-yyyy') and curso like '"+curso+"' and ano = "+ano)
        lstAlunos = list(lstAlunos)
        lstTickets = list(lstTickets)
        texto = u"""Foram contabilizados %s aluno(s) do %sº ano do %s para o dia %s""" % (len(lstConverter(lstTickets, lstAlunos)), ano, stCurso, dia)
    elif curso != u"" and curso != u"0":
        if curso == u"1":
            stCurso = "T&eacute;cnico Integrado em Inform&aacute;tica"
        else:
            stCurso = "T&eacute;cnico Integrado em Mecatr&ocirc;nica"

        lstAlunos = Refeicao.objects.raw("SELECT A.id, data, aluno_id FROM relatorio_refeicao R INNER JOIN aluno_aluno A on  A.id = R.aluno_id where data = to_date('"+dia+"', 'dd-mm-yyyy') and curso like '"+curso+"'")
        lstTickets = Ticket.objects.raw("SELECT A.id, data, aluno_id FROM relatorio_ticket R INNER JOIN aluno_aluno A on  A.id = R.aluno_id where data = to_date('"+dia+"', 'dd-mm-yyyy') and curso like '"+curso+"'")
        lstAlunos = list(lstAlunos)
        lstTickets = list(lstTickets)
        texto = u"""Foram contabilizados %s aluno(s) do curso de %s para o dia %s""" % (len(lstConverter(lstTickets, lstAlunos)),stCurso,dia)
    elif ano != u"" and ano != u"0":
        lstAlunos = Refeicao.objects.raw("SELECT A.id, data, aluno_id FROM relatorio_refeicao R INNER JOIN aluno_aluno A on  A.id = R.aluno_id where data = to_date('"+dia+"', 'dd-mm-yyyy') and ano = "+ano)
        lstTickets = Ticket.objects.raw("SELECT A.id, data, aluno_id FROM relatorio_ticket R INNER JOIN aluno_aluno A on  A.id = R.aluno_id where data = to_date('"+dia+"', 'dd-mm-yyyy') and ano = "+ano)
        lstAlunos = list(lstAlunos)
        lstTickets = list(lstTickets)
        texto = u"""Foram contabilizados %s aluno(s) do %sº ano para o dia %s""" % (len(lstConverter(lstTickets, lstAlunos)),ano,dia)
    else:
        lstAlunos = Refeicao.objects.raw("select * from relatorio_refeicao where data = to_date('"+dia+"', 'dd-mm-yyyy')")
        lstTickets = Ticket.objects.raw("select * from relatorio_ticket where data = to_date('"+dia+"', 'dd-mm-yyyy')")
        lstAlunos = list(lstAlunos)
        lstTickets = list(lstTickets)
        texto = u"""Foram contabilizados %s aluno(s) para o dia %s""" % (len(lstConverter(lstTickets, lstAlunos)),dia)

    lstAlunos = lstConverter(lstTickets, lstAlunos)

    # Estilos
    styles = getSampleStyleSheet()
    styles.add(ParagraphStyle(name='Justify', alignment=TA_JUSTIFY))
    styles.add(ParagraphStyle(name='Center', alignment=TA_CENTER))

    # Estória do arquivo
    Story = []

    cabecalho = u'Instituto Federal de Educação, Ciência e Tecnologia de São Paulo'
    titulo = u'Relatório de Refeições servidas no dia: '+dia

    item = Paragraph('<b>#</b>', styles['Center'])
    prontuario = Paragraph('<b>Prontuario</b>', styles['Center'])
    nome = Paragraph('<b>Nome</b>', styles['Center'])
    sobrenome = Paragraph('<b>Sobrenome</b>', styles['Center'])
    curso = Paragraph('<b>Curso</b>', styles['Center'])
    
    data = [[item,prontuario,nome,sobrenome,curso]]
    
    item = 0
    
    for aluno in lstAlunos:
        item+=1
        liItem = Paragraph(str(item), styles["Center"])
        prontuario = Paragraph(str(aluno.aluno.prontuario), styles["Center"])
        nome = Paragraph(aluno.aluno.nome, styles["Center"])
        sobrenome = Paragraph(aluno.aluno.sobrenome, styles["Center"])
        
        if aluno.aluno.curso == '1':
            curso = Paragraph('Técnico Integrado em Informática', styles["Center"])
            
        elif aluno.aluno.curso == '2':
            curso = Paragraph('Técnico Integrado em Mecatrônica', styles["Center"])
            
        linha = [liItem,prontuario,nome,sobrenome,curso]
        
        data.append(linha)
    
    tabela = Table(data)
    
    tabela.setStyle(TableStyle([('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
                                ('VALIGN', (0, 0), (-1, -1), 'MIDDLE'),
                                ('ALIGN', (0, 0), (-1, -1), 'CENTER'),
                                ('BOX', (0, 0), (-1, -1), 0.25, colors.black)]))
            
    footer = "Gerado em: "+datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S")
    
    Story.append(Paragraph(cabecalho, styles["Center"]))
    Story.append(Spacer(1, 0.4*inch))
    Story.append(Paragraph(titulo, styles["Title"]))
    Story.append(Spacer(1, 0.2*inch))
    Story.append(Paragraph(texto, styles["Justify"]))
    Story.append(Spacer(1, 0.2*inch))
    Story.append(tabela)
    Story.append(Spacer(1, 1.2*inch))
    Story.append(Paragraph(footer, styles["Center"]))

    pdf_file = tempfile.NamedTemporaryFile(mode='w+b', suffix=".pdf", delete=True)
    response = HttpResponse(pdf_file, content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename="Ref Servidas '+dia+'.pdf"'
    
    doc = SimpleDocTemplate(response)
    
    # Cria o documento
    doc.build(Story)

    return response

def dayTicPDF(request):
    dia = request.POST.get('data')
    
    lstAlunos = Refeicao.objects.raw("select * from relatorio_refeicao where data = to_date('"+dia+"', 'dd-mm-yyyy')")
    lstAlunos = list(lstAlunos)
    
    lstTickets = Ticket.objects.raw("select * from relatorio_ticket where data = to_date('"+dia+"', 'dd-mm-yyyy')")
    lstTickets = list(lstTickets)
    
    lstAlunos = comparaLista(lstTickets, lstAlunos)
    
    # Estilos
    styles = getSampleStyleSheet()
    styles.add(ParagraphStyle(name='Justify', alignment=TA_JUSTIFY))
    styles.add(ParagraphStyle(name='Center', alignment=TA_CENTER))
        
    # Estória do arquivo
    Story = []

    cabecalho = u'Instituto Federal de Educação, Ciência e Tecnologia de São Paulo'
    titulo = u'Relatório de Tickets gerados e não utilizados no dia: '+dia
    
    texto = u"""Foram contabilizados %s aluno(s) para o dia %s""" % (len(lstAlunos),dia)
    
    item = Paragraph('<b>#</b>', styles['Center'])
    prontuario = Paragraph('<b>Prontuario</b>', styles['Center'])
    nome = Paragraph('<b>Nome</b>', styles['Center'])
    sobrenome = Paragraph('<b>Sobrenome</b>', styles['Center'])
    curso = Paragraph('<b>Curso</b>', styles['Center'])
    
    data = [[item,prontuario,nome,sobrenome,curso]]
    
    item = 0
    
    for aluno in lstAlunos:
        item+=1
        liItem = Paragraph(str(item), styles["Center"])
        prontuario = Paragraph(str(aluno.aluno.prontuario), styles["Center"])
        nome = Paragraph(aluno.aluno.nome, styles["Center"])
        sobrenome = Paragraph(aluno.aluno.sobrenome, styles["Center"])
        
        if aluno.aluno.curso == '1':
            curso = Paragraph('Técnico Integrado em Informática', styles["Center"])
            
        elif aluno.aluno.curso == '2':
            curso = Paragraph('Técnico Integrado em Mecatrônica', styles["Center"])
            
        linha = [liItem,prontuario,nome,sobrenome,curso]
        
        data.append(linha)
    
    tabela = Table(data)
    
    tabela.setStyle(TableStyle([('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
                                ('VALIGN', (0, 0), (-1, -1), 'MIDDLE'),
                                ('ALIGN', (0, 0), (-1, -1), 'CENTER'),
                                ('BOX', (0, 0), (-1, -1), 0.25, colors.black)]))
    
        
    footer = "Gerado em: "+datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S")
    
    Story.append(Paragraph(cabecalho, styles["Center"]))
    Story.append(Spacer(1, 0.4*inch))
    Story.append(Paragraph(titulo, styles["Title"]))
    Story.append(Spacer(1, 0.2*inch))
    Story.append(Paragraph(texto, styles["Justify"]))
    Story.append(Spacer(1, 0.2*inch))
    Story.append(tabela)
    Story.append(Spacer(1, 1.2*inch))
    Story.append(Paragraph(footer, styles["Center"]))
    
    pdf_file = tempfile.NamedTemporaryFile(mode='w+b', suffix=".pdf", delete=True)
    response = HttpResponse(pdf_file, content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename="Tickets Inutilizados '+dia+'.pdf"'
    
    doc = SimpleDocTemplate(response)
    
    # Cria o documento
    doc.build(Story)

    return response
    
def dayBothPDF(request):
    render(dayRefPDF(request),dayTicPDF(request))

def monPDF(request):
    data = request.POST.get('data')
    total=0
    totalRef = 0
      
    aux = data.split("/")    
    ano = int(aux[1])
    mes = int(aux[0])
    
    qtdDias = monthrange(ano, mes)[1]  
    
    # Estilos
    styles = getSampleStyleSheet()
    styles.add(ParagraphStyle(name='Justify', alignment=TA_JUSTIFY))
    styles.add(ParagraphStyle(name='Center', alignment=TA_CENTER))
            
    # Estória do arquivo
    Story = []

    cabecalho = u'Instituto Federal de Educação, Ciência e Tecnologia de São Paulo'
    titulo = u'Relatório de Tickets para alimentação emitidos no mês: '+data
    
    day = Paragraph('<b>Dia</b>', styles['Center'])
    tic = Paragraph('<b>Tickets Gerados</b>', styles['Center'])
    ref = Paragraph('<b>Refeições Servidas</b>', styles['Center'])
    
    pdf = [[day,tic,ref]]
    
    for x in range(1,qtdDias+1):
        stData = str(x)+"-"+str(mes)+"-"+str(ano)
        weekDay = datetime.datetime(ano, mes, x).weekday()
       
        if weekDay != 5 and weekDay != 6:
            lstAlunos = Ticket.objects.raw("select * from relatorio_ticket where data = to_date('"+stData+"', 'dd-mm-yyyy')")
            lstAlunos = list(lstAlunos)
            lstRefeicoes = Refeicao.objects.raw("select * from relatorio_refeicao where data = to_date('"+stData+"', 'dd-mm-yyyy')")
            lstRefeicoes = list(lstRefeicoes)
            
            qtdAlunos = Paragraph(str(lstAlunos.__len__()), styles["Center"])
            qtdReficoes = Paragraph(str(lstRefeicoes.__len__()), styles["Center"])
            total = total+(lstAlunos.__len__())
            totalRef = totalRef+(lstRefeicoes.__len__())
            
            linha = [Paragraph(stData, styles["Center"]), qtdAlunos, qtdReficoes]
            pdf.append(linha)
    
    pdf.append([Paragraph('<b>Total</b>', styles["Center"]), 
                Paragraph('<b>'+str(total)+'</b>', styles["Center"]),
                Paragraph('<b>'+str(totalRef)+'</b>', styles["Center"])])
    
    tabela = Table(pdf)
    
    tabela.setStyle(TableStyle([('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
                                ('VALIGN', (0, 0), (-1, -1), 'MIDDLE'),
                                ('ALIGN', (0, 0), (-1, -1), 'CENTER'),
                                ('BOX', (0, 0), (-1, -1), 0.25, colors.black)]))
                   
    assinatura = "____________________________ &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp ____________________________"
    assTexto = "&nbsp &nbsp &nbsp &nbsp Fiscal de Contrato &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp Representante da Prestadora"
    
    footer = "Gerado em: "+datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S")
    
    Story.append(Paragraph(cabecalho, styles["Center"]))
    Story.append(Spacer(1, 0.4*inch))
    Story.append(Paragraph(titulo, styles["Title"]))
    Story.append(Spacer(1, 0.2*inch))
    Story.append(tabela)
    Story.append(Spacer(1, 0.8*inch))
    Story.append(Paragraph(assinatura, styles["Center"]))
    Story.append(Paragraph(assTexto, styles["Center"]))      
    Story.append(Spacer(1, 1.2*inch))
    Story.append(Paragraph(footer, styles["Center"]))
                     
    pdf_file = tempfile.NamedTemporaryFile(mode='w+b', suffix=".pdf", delete=True)
    response = HttpResponse(pdf_file, content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename="'+data+'.pdf"'
    
    doc = SimpleDocTemplate(response)
    
    # Cria o documento
    doc.build(Story)

    return response
