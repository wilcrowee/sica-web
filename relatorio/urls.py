# -*- coding: utf-8 -*-

from django.conf.urls import url
from relatorio import  views

urlpatterns = [
    url(r'^$', views.relatorios),
    url(r'^dayRefPDF/$', views.dayRefPDF),
    url(r'^dayTicPDF/$', views.dayTicPDF),
    url(r'^dayBothPDF/$', views.dayBothPDF),
    url(r'^monPDF/$', views.monPDF),
    url(r'^monXLS/$', views.monXLS),
    url(r'^ticket/$', views.consomeTicket)
]

