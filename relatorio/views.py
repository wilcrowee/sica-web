# -*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required, permission_required
from django.http import HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from models import Ticket
from curso.models import Curso
from calendar import monthrange

from reportlab.lib.enums import TA_CENTER, TA_JUSTIFY
from reportlab.lib.styles import ParagraphStyle, getSampleStyleSheet
from reportlab.platypus import (Paragraph, SimpleDocTemplate, Spacer, Table, TableStyle)
from reportlab.lib.units import inch
from reportlab.lib import colors

import datetime, tempfile, xlsxwriter, io
from aluno.models import Aluno
from relatorio.models import Refeicao
from operator import sub

@login_required(login_url='/accounts/login/')
def relatorios(request):
    data = datetime.datetime.now()
    itensTicket = Ticket.objects.raw("select * from relatorio_ticket where data = to_date('"+data.strftime("%Y-%m-%d")+"', 'yyyy-mm-dd')")
    itensRefeicao = Refeicao.objects.raw("select * from relatorio_refeicao where data = to_date('"+data.strftime("%Y-%m-%d")+"','yyyy-mm-dd')")
    itensTicket = len(list(itensTicket))
    itensRefeicao = len(list(itensRefeicao))
    cursos = Curso.objects.all()
    
    if request.method == 'POST':
        curso = request.POST.get('curso')
        ano = request.POST.get('ano')
        
        if request.POST.get("tipo") == '1':
            data = request.POST.get('data')
            
            if int(curso) != 0 and int(ano) != 0:
                lstAlunos = Refeicao.objects.raw("SELECT A.id, data, aluno_id FROM relatorio_refeicao R INNER JOIN aluno_aluno A on  A.id = R.aluno_id where data = to_date('"+data+"', 'dd-mm-yyyy') and curso like '"+curso+"' and ano = "+ano)
                lstTickets = Ticket.objects.raw("SELECT A.id, data, aluno_id FROM relatorio_ticket R INNER JOIN aluno_aluno A on  A.id = R.aluno_id where data = to_date('"+data+"', 'dd-mm-yyyy') and curso like '"+curso+"' and ano = "+ano)
            elif int(curso) != 0:
                lstAlunos = Refeicao.objects.raw("SELECT A.id, data, aluno_id FROM relatorio_refeicao R INNER JOIN aluno_aluno A on  A.id = R.aluno_id where data = to_date('"+data+"', 'dd-mm-yyyy') and curso like '"+curso+"'")
                lstTickets = Ticket.objects.raw("SELECT A.id, data, aluno_id FROM relatorio_ticket R INNER JOIN aluno_aluno A on  A.id = R.aluno_id where data = to_date('"+data+"', 'dd-mm-yyyy') and curso like '"+curso+"'")
            elif int(ano) != 0:
                lstAlunos = Refeicao.objects.raw("SELECT A.id, data, aluno_id FROM relatorio_refeicao R INNER JOIN aluno_aluno A on  A.id = R.aluno_id where data = to_date('"+data+"', 'dd-mm-yyyy') and ano = "+ano)
                lstTickets = Ticket.objects.raw("SELECT A.id, data, aluno_id FROM relatorio_ticket R INNER JOIN aluno_aluno A on  A.id = R.aluno_id where data = to_date('"+data+"', 'dd-mm-yyyy') and ano = "+ano)
            else:
                lstAlunos = Refeicao.objects.raw("select * from relatorio_refeicao where data = to_date('"+data+"', 'dd-mm-yyyy')")
                lstTickets = Ticket.objects.raw("select * from relatorio_ticket where data = to_date('"+data+"', 'dd-mm-yyyy')")
            
            lstAlunos = list(lstAlunos)
            lstTickets = list(lstTickets)
                
            lstAlunos = lstConverter(lstTickets, lstAlunos)
            lstTickets = comparaLista(lstTickets, lstAlunos)
            
            lstAlunos.sort(key=lambda x: x.aluno.nome, reverse=False)
            lstTickets.sort(key=lambda x: x.aluno.nome, reverse=False)
            
            return render(request, 'relatorio/relatorios.html', {'contadorRef': itensRefeicao, 'contador': itensTicket,'data': data,'contadorRefeicao': len(lstAlunos), 'contadorTicket': len(lstTickets), 'alunos': list(lstAlunos), 'tickets': list(lstTickets), 'id': 1, 'cursos': cursos, 'pdf_curso': curso, 'pdf_ano': ano})
        elif request.POST.get("tipo") == '2':
            data = request.POST.get('data')
            lstDay = []
            lstCount = []
            lstRef = []
            total = 0
            totalRef = 0
                 
            aux = data.split("/")    
            data_ano = int(aux[1])
            mes = int(aux[0])
            qtdDias = monthrange(data_ano, mes)[1]            
            
            for x in range(1,qtdDias+1):
                stData = str(x)+"-"+str(mes)+"-"+str(data_ano)
                weekDay = datetime.datetime(data_ano, mes, x).weekday()
                
                
                if weekDay != 5 and weekDay != 6:
                    if int(curso) != 0 and int(ano) != 0:
                        lstRefeicao = Refeicao.objects.raw("SELECT A.id, data, aluno_id FROM relatorio_refeicao R INNER JOIN aluno_aluno A on  A.id = R.aluno_id where data = to_date('"+stData+"', 'dd-mm-yyyy') and curso like '"+curso+"' and ano = "+ano)
                        lstAlunos = Ticket.objects.raw("SELECT A.id, data, aluno_id FROM relatorio_ticket R INNER JOIN aluno_aluno A on  A.id = R.aluno_id where data = to_date('"+stData+"', 'dd-mm-yyyy') and curso like '"+curso+"' and ano = "+ano)
                    elif int(curso) != 0:
                        lstRefeicao = Refeicao.objects.raw("SELECT A.id, data, aluno_id FROM relatorio_refeicao R INNER JOIN aluno_aluno A on  A.id = R.aluno_id where data = to_date('"+stData+"', 'dd-mm-yyyy') and curso like '"+curso+"'")
                        lstAlunos = Ticket.objects.raw("SELECT A.id, data, aluno_id FROM relatorio_ticket R INNER JOIN aluno_aluno A on  A.id = R.aluno_id where data = to_date('"+stData+"', 'dd-mm-yyyy') and curso like '"+curso+"'")
                    elif int(ano) != 0:
                        lstRefeicao = Refeicao.objects.raw("SELECT A.id, data, aluno_id FROM relatorio_refeicao R INNER JOIN aluno_aluno A on  A.id = R.aluno_id where data = to_date('"+stData+"', 'dd-mm-yyyy') and ano = "+ano)
                        lstAlunos = Ticket.objects.raw("SELECT A.id, data, aluno_id FROM relatorio_ticket R INNER JOIN aluno_aluno A on  A.id = R.aluno_id where data = to_date('"+stData+"', 'dd-mm-yyyy') and ano = "+ano)
                    else:
                        lstRefeicao = Refeicao.objects.raw("select * from relatorio_refeicao where data = to_date('"+stData+"', 'dd-mm-yyyy')")
                        lstAlunos = Ticket.objects.raw("select * from relatorio_ticket where data = to_date('"+stData+"', 'dd-mm-yyyy')")
            
                    lstRefeicao = list(lstRefeicao)
                    lstAlunos = list(lstAlunos)
                    lstDay.append(stData)
                    lstCount.append(lstAlunos.__len__())
                    total = total+(lstAlunos.__len__())
                    lstRef.append(len(lstConverter(lstAlunos, lstRefeicao)))
                    totalRef = totalRef+(lstRefeicao.__len__())
                    
                lstTable = zip(lstDay,lstCount, lstRef)
                                
            return render(request, 'relatorio/relatorios.html', {'contadorRef': itensRefeicao, 'contador': itensTicket, 'id': 2, 'cursos': cursos, 'lstTable': lstTable, 'mes': mes, 'ano': data_ano, 'total': total, 'pdf_curso': curso, 'pdf_ano': ano})
    else:
        if '0' in request.session:
            msg = request.session['0']
            del request.session['0']
            
            return render(request, 'relatorio/relatorios.html', {'contadorRef': itensRefeicao, 'contador': itensTicket,'id': 1, 'cursos': cursos, 'msg': msg})
        
        return render(request, 'relatorio/relatorios.html', {'contadorRef': itensRefeicao, 'contador': itensTicket,'id': 1, 'cursos': cursos})

@login_required(login_url='/accounts/login/')
@permission_required('aluno.add_aluno', raise_exception=True)
def consomeTicket(request):
    if request.method == 'POST':
        dia = request.POST.get('data')
        enviar = request.POST.get('button')
            
        lstAlunos = Refeicao.objects.raw("select * from relatorio_refeicao where data = to_date('"+dia+"', 'dd-mm-yyyy')")
        lstAlunos = list(lstAlunos)
        lstTickets = Ticket.objects.raw("select * from relatorio_ticket where data = to_date('"+dia+"', 'dd-mm-yyyy')")
        lstTickets = list(lstTickets)
    
        lstAlunos = comparaLista(lstTickets, lstAlunos)
        lstAlunos.sort(key=lambda x: x.aluno.nome, reverse=False)
                
        if enviar:
            lista = request.POST.getlist('lista')
            
            if len(lista) != 0:
                dia = datetime.datetime.strptime(dia, '%d/%m/%Y')
                
                for item in lista:
                    aluno = get_object_or_404(Aluno, pk = item)
                    
                    ref = Refeicao(aluno = aluno,
                                   data = dia)
                    
                    ref.save()
                    
                request.session[0] = "Tickets consumidos com sucesso"
                
                return redirect('/relatorio')
            else:
                return render(request, 'relatorio/refeicao.html', {'tickets': lstAlunos, 'dia': dia, 'err': "Nenhum registro selecionado"})
        
        return render(request, 'relatorio/refeicao.html', {'tickets': lstAlunos, 'dia': dia})
    else:
        return redirect('/relatorio')
    
def comparaLista(tickets, refeicoes):
    aux = []
       
    for tic in tickets:
        count = 0
        
        for ref in refeicoes:
            if tic.aluno.prontuario == ref.aluno.prontuario:
                count = count+1
        
        if count == 0:
            aux.append(tic)
    
    return aux

def lstConverter(tickets, refeicoes):
    aux = []
    
    for tic in tickets:
        count = 0
        for ref in refeicoes:
            if tic.aluno.prontuario == ref.aluno.prontuario:
                count = count+1
                if(count == 1):
                    aux.append(tic)
    
    return aux
        
def dayRefPDF(request):
    dia = request.POST.get('data')
    curso = request.POST.get('pdf_curso')
    ano = request.POST.get('pdf_ano')
    
    if curso != u"" and  ano != u"" and curso != u"0" and  ano != u"0":
        if curso == u"1":
            stCurso = "T&eacute;cnico Integrado em Inform&aacute;tica"
        else:
            stCurso = "T&eacute;cnico Integrado em Mecatr&ocirc;nica"
        
        lstAlunos = Refeicao.objects.raw("SELECT A.id, data, aluno_id FROM relatorio_refeicao R INNER JOIN aluno_aluno A on  A.id = R.aluno_id where data = to_date('"+dia+"', 'dd-mm-yyyy') and curso like '"+curso+"' and ano = "+ano)
        lstTickets = Ticket.objects.raw("SELECT A.id, data, aluno_id FROM relatorio_ticket R INNER JOIN aluno_aluno A on  A.id = R.aluno_id where data = to_date('"+dia+"', 'dd-mm-yyyy') and curso like '"+curso+"' and ano = "+ano)
        lstAlunos = list(lstAlunos)
        lstTickets = list(lstTickets)
        texto = u"""Foram contabilizados %s aluno(s) do %sº ano do %s para o dia %s""" % (len(lstConverter(lstTickets, lstAlunos)), ano, stCurso, dia)
    elif curso != u"" and curso != u"0":
        if curso == u"1":
            stCurso = "T&eacute;cnico Integrado em Inform&aacute;tica"
        else:
            stCurso = "T&eacute;cnico Integrado em Mecatr&ocirc;nica"
        
        
        lstAlunos = Refeicao.objects.raw("SELECT A.id, data, aluno_id FROM relatorio_refeicao R INNER JOIN aluno_aluno A on  A.id = R.aluno_id where data = to_date('"+dia+"', 'dd-mm-yyyy') and curso like '"+curso+"'")
        lstTickets = Ticket.objects.raw("SELECT A.id, data, aluno_id FROM relatorio_ticket R INNER JOIN aluno_aluno A on  A.id = R.aluno_id where data = to_date('"+dia+"', 'dd-mm-yyyy') and curso like '"+curso+"'")
        lstAlunos = list(lstAlunos)
        lstTickets = list(lstTickets)
        texto = u"""Foram contabilizados %s aluno(s) do curso de %s para o dia %s""" % (len(lstConverter(lstTickets, lstAlunos)),stCurso,dia)
    elif ano != u"" and ano != u"0":
        lstAlunos = Refeicao.objects.raw("SELECT A.id, data, aluno_id FROM relatorio_refeicao R INNER JOIN aluno_aluno A on  A.id = R.aluno_id where data = to_date('"+dia+"', 'dd-mm-yyyy') and ano = "+ano)
        lstTickets = Ticket.objects.raw("SELECT A.id, data, aluno_id FROM relatorio_ticket R INNER JOIN aluno_aluno A on  A.id = R.aluno_id where data = to_date('"+dia+"', 'dd-mm-yyyy') and ano = "+ano)
        lstAlunos = list(lstAlunos)
        lstTickets = list(lstTickets)
        texto = u"""Foram contabilizados %s aluno(s) do %sº ano para o dia %s""" % (len(lstConverter(lstTickets, lstAlunos)),ano,dia)
    else:
        lstAlunos = Refeicao.objects.raw("select * from relatorio_refeicao where data = to_date('"+dia+"', 'dd-mm-yyyy')")
        lstTickets = Ticket.objects.raw("select * from relatorio_ticket where data = to_date('"+dia+"', 'dd-mm-yyyy')")
        lstAlunos = list(lstAlunos)
        lstTickets = list(lstTickets)
        texto = u"""Foram contabilizados %s aluno(s) para o dia %s""" % (len(lstConverter(lstTickets, lstAlunos)),dia)
        
    lstAlunos = lstConverter(lstTickets, lstAlunos)
    
    # Estilos
    styles = getSampleStyleSheet()
    styles.add(ParagraphStyle(name='Justify', alignment=TA_JUSTIFY))
    styles.add(ParagraphStyle(name='Center', alignment=TA_CENTER))
        
    # Estória do arquivo
    Story = []

    cabecalho = u'Instituto Federal de Educação, Ciência e Tecnologia de São Paulo'
    titulo = u'Relatório de Refeições servidas no dia: '+dia
    
    item = Paragraph('<b>#</b>', styles['Center'])
    prontuario = Paragraph('<b>Prontuario</b>', styles['Center'])
    nome = Paragraph('<b>Nome</b>', styles['Center'])
    sobrenome = Paragraph('<b>Sobrenome</b>', styles['Center'])
    curso = Paragraph('<b>Curso</b>', styles['Center'])
    
    data = [[item,prontuario,nome,sobrenome,curso]]
    
    item = 0
    
    for aluno in lstAlunos:
        item+=1
        liItem = Paragraph(str(item), styles["Center"])
        prontuario = Paragraph(str(aluno.aluno.prontuario), styles["Center"])
        nome = Paragraph(aluno.aluno.nome, styles["Center"])
        sobrenome = Paragraph(aluno.aluno.sobrenome, styles["Center"])
        
        if aluno.aluno.curso == '1':
            curso = Paragraph('Técnico Integrado em Informática', styles["Center"])
            
        elif aluno.aluno.curso == '2':
            curso = Paragraph('Técnico Integrado em Mecatrônica', styles["Center"])
            
        linha = [liItem,prontuario,nome,sobrenome,curso]
        
        data.append(linha)
    
    tabela = Table(data)
    
    tabela.setStyle(TableStyle([('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
                                ('VALIGN', (0, 0), (-1, -1), 'MIDDLE'),
                                ('ALIGN', (0, 0), (-1, -1), 'CENTER'),
                                ('BOX', (0, 0), (-1, -1), 0.25, colors.black)]))
            
    footer = "Gerado em: "+datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S")
    
    Story.append(Paragraph(cabecalho, styles["Center"]))
    Story.append(Spacer(1, 0.4*inch))
    Story.append(Paragraph(titulo, styles["Title"]))
    Story.append(Spacer(1, 0.2*inch))
    Story.append(Paragraph(texto, styles["Justify"]))
    Story.append(Spacer(1, 0.2*inch))
    Story.append(tabela)
    Story.append(Spacer(1, 1.2*inch))
    Story.append(Paragraph(footer, styles["Center"]))

    pdf_file = tempfile.NamedTemporaryFile(mode='w+b', suffix=".pdf", delete=True)
    response = HttpResponse(pdf_file, content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename="Ref Servidas '+dia+'.pdf"'
    
    doc = SimpleDocTemplate(response)
    
    # Cria o documento
    doc.build(Story)

    return response

def dayTicPDF(request):
    dia = request.POST.get('data')
    curso = request.POST.get('pdf_curso')
    ano = request.POST.get('pdf_ano')
        
    if curso != u"" and  ano != u"" and curso != u"0" and  ano != u"0":
        if curso == u"1":
            stCurso = "T&eacute;cnico Integrado em Inform&aacute;tica"
        else:
            stCurso = "T&eacute;cnico Integrado em Mecatr&ocirc;nica"
        
        lstAlunos = Refeicao.objects.raw("SELECT A.id, data, aluno_id FROM relatorio_refeicao R INNER JOIN aluno_aluno A on  A.id = R.aluno_id where data = to_date('"+dia+"', 'dd-mm-yyyy') and curso like '"+curso+"' and ano = "+ano)
        lstTickets = Ticket.objects.raw("SELECT A.id, data, aluno_id FROM relatorio_ticket R INNER JOIN aluno_aluno A on  A.id = R.aluno_id where data = to_date('"+dia+"', 'dd-mm-yyyy') and curso like '"+curso+"' and ano = "+ano)
        lstAlunos = list(lstAlunos)
        lstTickets = list(lstTickets)
        lstAlunos = comparaLista(lstTickets, lstAlunos)
        texto = u"""Foram contabilizados %s aluno(s) do %sº ano do %s para o dia %s""" % (len(lstAlunos), ano, stCurso, dia)
    elif curso != u"" and curso != u"0":
        if curso == u"1":
            stCurso = "T&eacute;cnico Integrado em Inform&aacute;tica"
        else:
            stCurso = "T&eacute;cnico Integrado em Mecatr&ocirc;nica"
        
        
        lstAlunos = Refeicao.objects.raw("SELECT A.id, data, aluno_id FROM relatorio_refeicao R INNER JOIN aluno_aluno A on  A.id = R.aluno_id where data = to_date('"+dia+"', 'dd-mm-yyyy') and curso like '"+curso+"'")
        lstTickets = Ticket.objects.raw("SELECT A.id, data, aluno_id FROM relatorio_ticket R INNER JOIN aluno_aluno A on  A.id = R.aluno_id where data = to_date('"+dia+"', 'dd-mm-yyyy') and curso like '"+curso+"'")
        lstAlunos = list(lstAlunos)
        lstTickets = list(lstTickets)
        lstAlunos = comparaLista(lstTickets, lstAlunos)
        texto = u"""Foram contabilizados %s aluno(s) do curso de %s para o dia %s""" % (len(lstAlunos),stCurso,dia)
    elif ano != u"" and ano != u"0":
        lstAlunos = Refeicao.objects.raw("SELECT A.id, data, aluno_id FROM relatorio_refeicao R INNER JOIN aluno_aluno A on  A.id = R.aluno_id where data = to_date('"+dia+"', 'dd-mm-yyyy') and ano = "+ano)
        lstTickets = Ticket.objects.raw("SELECT A.id, data, aluno_id FROM relatorio_ticket R INNER JOIN aluno_aluno A on  A.id = R.aluno_id where data = to_date('"+dia+"', 'dd-mm-yyyy') and ano = "+ano)
        lstAlunos = list(lstAlunos)
        lstTickets = list(lstTickets)
        lstAlunos = comparaLista(lstTickets, lstAlunos)
        texto = u"""Foram contabilizados %s aluno(s) do %sº ano para o dia %s""" % (len(lstAlunos),ano,dia)
    else:
        lstAlunos = Refeicao.objects.raw("select * from relatorio_refeicao where data = to_date('"+dia+"', 'dd-mm-yyyy')")
        lstTickets = Ticket.objects.raw("select * from relatorio_ticket where data = to_date('"+dia+"', 'dd-mm-yyyy')")
        lstAlunos = list(lstAlunos)
        lstTickets = list(lstTickets)
        lstAlunos = comparaLista(lstTickets, lstAlunos)
        texto = u"""Foram contabilizados %s aluno(s) para o dia %s""" % (len(lstAlunos),dia)
    
    # Estilos
    styles = getSampleStyleSheet()
    styles.add(ParagraphStyle(name='Justify', alignment=TA_JUSTIFY))
    styles.add(ParagraphStyle(name='Center', alignment=TA_CENTER))
        
    # Estória do arquivo
    Story = []

    cabecalho = u'Instituto Federal de Educação, Ciência e Tecnologia de São Paulo'
    titulo = u'Relatório de Tickets gerados e não utilizados no dia: '+dia
    
    item = Paragraph('<b>#</b>', styles['Center'])
    prontuario = Paragraph('<b>Prontuario</b>', styles['Center'])
    nome = Paragraph('<b>Nome</b>', styles['Center'])
    sobrenome = Paragraph('<b>Sobrenome</b>', styles['Center'])
    curso = Paragraph('<b>Curso</b>', styles['Center'])
    
    data = [[item,prontuario,nome,sobrenome,curso]]
    
    item = 0
    
    for aluno in lstAlunos:
        item+=1
        liItem = Paragraph(str(item), styles["Center"])
        prontuario = Paragraph(str(aluno.aluno.prontuario), styles["Center"])
        nome = Paragraph(aluno.aluno.nome, styles["Center"])
        sobrenome = Paragraph(aluno.aluno.sobrenome, styles["Center"])
        
        if aluno.aluno.curso == '1':
            curso = Paragraph('Técnico Integrado em Informática', styles["Center"])
            
        elif aluno.aluno.curso == '2':
            curso = Paragraph('Técnico Integrado em Mecatrônica', styles["Center"])
            
        linha = [liItem,prontuario,nome,sobrenome,curso]
        
        data.append(linha)
    
    tabela = Table(data)
    
    tabela.setStyle(TableStyle([('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
                                ('VALIGN', (0, 0), (-1, -1), 'MIDDLE'),
                                ('ALIGN', (0, 0), (-1, -1), 'CENTER'),
                                ('BOX', (0, 0), (-1, -1), 0.25, colors.black)]))
    
        
    footer = "Gerado em: "+datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S")
    
    Story.append(Paragraph(cabecalho, styles["Center"]))
    Story.append(Spacer(1, 0.4*inch))
    Story.append(Paragraph(titulo, styles["Title"]))
    Story.append(Spacer(1, 0.2*inch))
    Story.append(Paragraph(texto, styles["Justify"]))
    Story.append(Spacer(1, 0.2*inch))
    Story.append(tabela)
    Story.append(Spacer(1, 1.2*inch))
    Story.append(Paragraph(footer, styles["Center"]))
    
    pdf_file = tempfile.NamedTemporaryFile(mode='w+b', suffix=".pdf", delete=True)
    response = HttpResponse(pdf_file, content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename="Tickets Inutilizados '+dia+'.pdf"'
    
    doc = SimpleDocTemplate(response)
    
    # Cria o documento
    doc.build(Story)

    return response
    
def dayBothPDF(request):
    render(dayRefPDF(request),dayTicPDF(request))

def monPDF(request):
    data = request.POST.get('data')
    curso = request.POST.get('pdf_curso')
    pdfAno = request.POST.get('pdf_ano')
    total=0
    totalRef = 0
    
    if curso != u'0' and curso != u'':
        if int(curso) == 1:
            stCurso = u'Técnico Integrado em Informática'
        else:
            stCurso = u'Técnico Integrado em Mecatrônica'
      
    aux = data.split("/")    
    ano = int(aux[1])
    mes = int(aux[0])
    
    qtdDias = monthrange(ano, mes)[1]  
    
    # Estilos
    styles = getSampleStyleSheet()
    styles.add(ParagraphStyle(name='Justify', alignment=TA_JUSTIFY))
    styles.add(ParagraphStyle(name='Center', alignment=TA_CENTER))
            
    # Estória do arquivo
    Story = []

    cabecalho = u'Instituto Federal de Educação, Ciência e Tecnologia de São Paulo'
    titulo = u'Relatório de Tickets para alimentação emitidos no mês: '+data
    
    day = Paragraph('<b>Dia</b>', styles['Center'])
    tic = Paragraph('<b>Tickets Gerados</b>', styles['Center'])
    ref = Paragraph('<b>Refeições Servidas</b>', styles['Center'])
    
    pdf = [[day,tic,ref]]
    
    for x in range(1,qtdDias+1):
        stData = str(x)+"-"+str(mes)+"-"+str(ano)
        weekDay = datetime.datetime(ano, mes, x).weekday()
       
        if weekDay != 5 and weekDay != 6:
            if int(curso) != 0 and int(pdfAno) != 0:
                lstRefeicoes = Refeicao.objects.raw("SELECT A.id, data, aluno_id FROM relatorio_refeicao R INNER JOIN aluno_aluno A on  A.id = R.aluno_id where data = to_date('"+stData+"', 'dd-mm-yyyy') and curso like '"+curso+"' and ano = "+pdfAno)
                lstAlunos = Ticket.objects.raw("SELECT A.id, data, aluno_id FROM relatorio_ticket R INNER JOIN aluno_aluno A on  A.id = R.aluno_id where data = to_date('"+stData+"', 'dd-mm-yyyy') and curso like '"+curso+"' and ano = "+pdfAno)
                titulo = u'Relatório de Tickets para alimentação emitidos para os alunos do '+pdfAno+u" do curso de "+stCurso+u' no mês:'+data
            elif int(curso) != 0:
                lstRefeicoes = Refeicao.objects.raw("SELECT A.id, data, aluno_id FROM relatorio_refeicao R INNER JOIN aluno_aluno A on  A.id = R.aluno_id where data = to_date('"+stData+"', 'dd-mm-yyyy') and curso like '"+curso+"'")
                lstAlunos = Ticket.objects.raw("SELECT A.id, data, aluno_id FROM relatorio_ticket R INNER JOIN aluno_aluno A on  A.id = R.aluno_id where data = to_date('"+stData+"', 'dd-mm-yyyy') and curso like '"+curso+"'")
                titulo = u'Relatório de Tickets para alimentação emitidos para os alunos do curso de '+stCurso+u' no mês:'+data
            elif int(pdfAno) != 0:
                lstRefeicoes = Refeicao.objects.raw("SELECT A.id, data, aluno_id FROM relatorio_refeicao R INNER JOIN aluno_aluno A on  A.id = R.aluno_id where data = to_date('"+stData+"', 'dd-mm-yyyy') and ano = "+pdfAno)
                lstAlunos = Ticket.objects.raw("SELECT A.id, data, aluno_id FROM relatorio_ticket R INNER JOIN aluno_aluno A on  A.id = R.aluno_id where data = to_date('"+stData+"', 'dd-mm-yyyy') and ano = "+pdfAno)
                titulo = u'Relatório de Tickets para alimentação emitidos para todos os alunos do '+pdfAno+u'º ano no mês:'+data
            else:
                lstRefeicoes = Refeicao.objects.raw("select * from relatorio_refeicao where data = to_date('"+stData+"', 'dd-mm-yyyy')")
                lstAlunos = Ticket.objects.raw("select * from relatorio_ticket where data = to_date('"+stData+"', 'dd-mm-yyyy')")
                titulo = u'Relatório de Tickets para alimentação emitidos no mês: '+data
                    
            lstAlunos = list(lstAlunos)
            lstRefeicoes = list(lstRefeicoes)
            
            qtdAlunos = Paragraph(str(lstAlunos.__len__()), styles["Center"])
            qtdReficoes = Paragraph(str(len(lstConverter(lstAlunos, lstRefeicoes))), styles["Center"])
            total = total+(lstAlunos.__len__())
            totalRef = totalRef+(lstRefeicoes.__len__())
            
            linha = [Paragraph(stData, styles["Center"]), qtdAlunos, qtdReficoes]
            pdf.append(linha)
    
    pdf.append([Paragraph('<b>Total</b>', styles["Center"]), 
                Paragraph('<b>'+str(total)+'</b>', styles["Center"]),
                Paragraph('<b>'+str(totalRef)+'</b>', styles["Center"])])
    
    tabela = Table(pdf)
    
    tabela.setStyle(TableStyle([('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
                                ('VALIGN', (0, 0), (-1, -1), 'MIDDLE'),
                                ('ALIGN', (0, 0), (-1, -1), 'CENTER'),
                                ('BOX', (0, 0), (-1, -1), 0.25, colors.black)]))
                   
    assinatura = "____________________________ &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp ____________________________"
    assTexto = "&nbsp &nbsp &nbsp &nbsp Fiscal de Contrato &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp Representante da Prestadora"
    
    footer = "Gerado em: "+datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S")
    
    Story.append(Paragraph(cabecalho, styles["Center"]))
    Story.append(Spacer(1, 0.4*inch))
    Story.append(Paragraph(titulo, styles["Title"]))
    Story.append(Spacer(1, 0.2*inch))
    Story.append(tabela)
    Story.append(Spacer(1, 0.6*inch))
    Story.append(Paragraph(assinatura, styles["Center"]))
    Story.append(Paragraph(assTexto, styles["Center"]))      
    Story.append(Spacer(1, 0.2*inch))
    Story.append(Paragraph(footer, styles["Center"]))
                     
    pdf_file = tempfile.NamedTemporaryFile(mode='w+b', suffix=".pdf", delete=True)
    response = HttpResponse(pdf_file, content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename="'+data+'.pdf"'
    
    doc = SimpleDocTemplate(response)
    
    # Cria o documento
    doc.build(Story)

    return response

def monXLS(request):
    data = request.POST.get('data')
    total=0
    totalRef = 0
    row = 4
      
    aux = data.split("/")    
    ano = int(aux[1])
    mes = int(aux[0])
    
    qtdDias = monthrange(ano, mes)[1]  
    
    output = io.BytesIO()
    workbook = xlsxwriter.Workbook(output)
    worksheet = workbook.add_worksheet(str(mes)+'-'+str(ano))
    
    header = "Relatório de Tickets para alimentação emitidos no mês: "+str(data)
    header = header.decode("UTF-8")
    
    titulo = workbook.add_format({
        'bold': 1,
        'align': 'center',
        'valign': 'vcenter',
        'font_size': 14})
    
    subTitulo = workbook.add_format({
        'bold': 1,
        'align': 'center',
        'valign': 'vcenter',
        })
     
    centralizado = workbook.add_format({
        'align': 'center',
        'valign': 'vcenter'})
    
    worksheet.merge_range('A1:C1', header, titulo)
    worksheet.write('A3', 'Dia', subTitulo)
    worksheet.set_column('A:A', 26)
    worksheet.write('B3', 'Tickets Gerados', subTitulo)
    worksheet.set_column('B:B', 26)
    worksheet.write('C3', 'Refeições Servidas'.decode("UTF-8"), subTitulo)
    worksheet.set_column('C:C', 26)
               
    for x in range(1,qtdDias+1):
        stData = str(x)+"-"+str(mes)+"-"+str(ano)
        weekDay = datetime.datetime(ano, mes, x).weekday()
                      
        if weekDay != 5 and weekDay != 6:
            lstAlunos = Ticket.objects.raw("select * from relatorio_ticket where data = to_date('"+stData+"', 'dd-mm-yyyy')")
            lstAlunos = list(lstAlunos)
            lstRefeicoes = Refeicao.objects.raw("select * from relatorio_refeicao where data = to_date('"+stData+"', 'dd-mm-yyyy')")
            lstRefeicoes = list(lstRefeicoes)
            
            worksheet.write('A'+str(row), stData, centralizado)
            worksheet.write_number('B'+str(row), lstAlunos.__len__(), centralizado)
            worksheet.write_number('C'+str(row), lstRefeicoes.__len__(), centralizado)
            row += 1      
        
    worksheet.write('A'+str(row), 'Total', subTitulo)
    worksheet.write_array_formula('B'+str(row), '{=SUM(B4:B'+str(row-1)+')}', centralizado)
    worksheet.write_array_formula('C'+str(row), '{=SUM(C4:C'+str(row-1)+')}', centralizado)
    
    row += 2
    
    worksheet.write('A'+str(row), '_______________________', centralizado)
    worksheet.write('C'+str(row), '_______________________', centralizado)
    
    row += 1
    
    worksheet.write('A'+str(row), 'Fiscal de Contrato', centralizado)
    worksheet.write('C'+str(row), 'Representante da Prestadora', centralizado)
    
    row += 2
    
    footer = "Gerado em: "+datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S")
    worksheet.merge_range('A'+str(row)+':'+'C'+str(row), footer, centralizado)
    
    workbook.close()
    output.seek(0)
    
    filename = str(mes)+'-'+str(ano)+'.xlsx'
    
    response = HttpResponse(
        output,
        content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    )
    
    response['Content-Disposition'] = 'attachment; filename=%s' % filename

    return response    