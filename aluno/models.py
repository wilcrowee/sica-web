# -*- coding: utf-8 -*-

from django.db import models 
from django.db.models.fields import CharField

class Aluno(models.Model):
    prontuario = models.CharField(max_length=30)
    nome = CharField(max_length=30)
    sobrenome = CharField(max_length=255)
    curso = CharField(max_length=2, default='0')
    ano = models.IntegerField()
    ativo = models.BooleanField(default=True)
    senha = CharField(max_length=30)   
 
    data_criacao = models.DateTimeField(u'Data de Criação', auto_now_add=True)

class Search(models.Model):
    campo = CharField(max_length=255)

class UploadCsv(models.Model):
    titulo = models.CharField(max_length=100)
    curso = models.CharField(max_length=2, default='0')
    arquivo = models.FileField()
     
    data_criacao = models.DateTimeField(u'Data de Criação', auto_now_add=True)
        
