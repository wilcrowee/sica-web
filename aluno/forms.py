# -*- coding: utf-8 -*-

from django import forms
from aluno.models import Aluno, UploadCsv

class AlunoForm(forms.ModelForm):

    class Meta:
        model = Aluno
        fields = ('prontuario','nome','sobrenome','curso','ano',)
    
    def clean(self):
        super(AlunoForm, self).clean()
        prontuario = self.cleaned_data.get('prontuario')
        curso = self.cleaned_data.get('curso')
        ano = self.cleaned_data.get('ano')
        
        aluno = Aluno.objects.filter(prontuario=prontuario)
              
        if self.errors:
            return self.cleaned_data
        
        if len(prontuario) != 7:
            self.add_error('prontuario', u'O Prontuario deve ter 7 digitos')
            
        if int(curso) == 0:
            self.add_error('curso', u'Um curso válido deve ser selecionado')
        
	if int(ano) == 0:
	    self.add_error('ano', u'Um ano válido deve ser selecionado')

	if aluno:
            self.add_error('prontuario', u'O Prontuario já foi registrado em outro aluno')
                
        return self.cleaned_data

class EditAlunoForm(forms.ModelForm):
    
    class Meta:
        model = Aluno
        fields = ('prontuario','nome','sobrenome','curso','ano', 'id',)
    
    def clean(self):
        super(EditAlunoForm, self).clean()
        prontuario = self.cleaned_data.get('prontuario')
        curso = self.cleaned_data.get('curso')
        id = self.initial.get('id')
        ano = self.cleaned_data.get('ano')
        
        aluno_new = Aluno.objects.filter(prontuario=prontuario)
        aluno_old = Aluno.objects.filter(id=id)
                 
        if self.errors:
            return self.cleaned_data
        
        if len(prontuario) != 7:
            self.add_error('prontuario', u'O Prontuario deve ter 7 digitos')
            
        if int(curso) == 0:
            self.add_error('curso', u'Um curso válido deve ser selecionado')
        
        if int(ano) == 0:
            self.add_error('ano', u'Um ano válido deve ser selecionado')
        
        if aluno_new:
            aluno_new = aluno_new[0]
            if aluno_old:
                aluno_old = aluno_old[0]
                if aluno_new.prontuario != aluno_old.prontuario:
                    self.add_error('prontuario', u'O Prontuario já foi registrado em outro aluno')
        
        return self.cleaned_data
           
class UploadFileForm(forms.ModelForm):
    
    class Meta:
        model = UploadCsv
        fields = ('curso',)
    
    def clean(self):
        super(UploadFileForm, self).clean()
        arquivo = self.initial.get('arquivo')
        curso = self.cleaned_data.get('curso')
        
        if not arquivo._name.split(".")[1] == 'csv':
            self.add_error('curso', u'Formato de Arquivo Inválido.')
        
        if int(curso) == 0:
            self.add_error('curso', u'Um curso válido deve ser selecionado')
        
        return self.cleaned_data

class SenhaForm(forms.ModelForm):
    senhaConfirm = forms.CharField(max_length=30, widget=forms.PasswordInput())
    
    class Meta:
        model = Aluno
        fields = ('senha',)
                
    def clean(self):
        super(SenhaForm, self).clean()
        senha = self.cleaned_data.get('senha')
        senhaConfirm = self.cleaned_data.get('senhaConfirm')
        
        if senha != senhaConfirm:
            self.add_error('senha', u'As senhas deve ser iguais.')
            self.add_error('senhaConfirm', u'As senhas deve ser iguais.')
        else:
            if len(senha) < 8:
                self.add_error('senha', u'A senha deve ter mais de 8 digitos.')
        
        return self.cleaned_data

