# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2018-07-16 18:17
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('aluno', '0004_aluno_ano'),
    ]

    operations = [
        migrations.AlterField(
            model_name='aluno',
            name='curso',
            field=models.CharField(default=b'0', max_length=2),
        ),
    ]
