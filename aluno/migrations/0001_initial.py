# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Aluno',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('prontuario', models.IntegerField()),
                ('nome', models.CharField(max_length=30)),
                ('sobrenome', models.CharField(max_length=255)),
                ('curso', models.CharField(default=b'1', max_length=2, choices=[(b'1', b'T\xc3\xa9cnico Integrado em Inform\xc3\xa1tica'), (b'2', b'T\xc3\xa9cnico Integrado em Mecatr\xc3\xb4nica')])),
                ('data_criacao', models.DateTimeField(auto_now_add=True, verbose_name='Data de Cria\xe7\xe3o')),
            ],
        ),
    ]
