# -*- coding: utf-8 -*-

from django.contrib import admin
from aluno.models import Aluno
from aluno.forms import AlunoForm

class AlunoAdmin(admin.ModelAdmin):
    list_display = ('id','prontuario','nome', 'sobrenome')
    ordering = ('id','nome','sobrenome')
    search_fields = ('prontuario','nome', 'sobrenome')
    list_display_icons = True
    export_to_xls = True
    form = AlunoForm
    
admin.site.register(Aluno, AlunoAdmin)

