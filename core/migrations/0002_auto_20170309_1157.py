# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2017-03-09 14:57
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Cardapio',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('principal', models.TextField(max_length=255)),
                ('salada', models.CharField(max_length=30)),
                ('sobremesa', models.CharField(max_length=30)),
                ('suco', models.CharField(max_length=30)),
                ('data', models.DateField()),
            ],
        ),
        migrations.RemoveField(
            model_name='horario',
            name='aluno',
        ),
        migrations.DeleteModel(
            name='Aluno',
        ),
        migrations.DeleteModel(
            name='Horario',
        ),
    ]
