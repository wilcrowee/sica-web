# -*- coding: utf-8 -*-

from django.template import RequestContext
from django.contrib.auth import authenticate, logout, login as auth_login
from django.shortcuts import redirect, render, render_to_response, get_object_or_404
from django.contrib.auth.decorators import login_required, permission_required
from core.models import Cardapio
from aluno.views import paginador, Aluno
from core.forms import CardapioForm, EditarCardapioForm
import datetime
from horario.models import Horario
from relatorio.models import Ticket

def accountsLogin(request):
    if request.user.is_active:
        return home(request)
    else:
        if '0'in request.session:
            msg = request.session['0']
            del request.session['0']
        else:
            msg = "Login Necessário"
        return render(request, 'index.html', {'msg_err': msg})
    
def accountsPassword(request):
    if request.method == 'POST':
        usuario = request.POST.get("form-username")
        oldPassword = request.POST.get("form-oldpassword")
        newPassword = request.POST.get("form-newpassword")
        
        if oldPassword == newPassword:
            return render(request, 'password.html', {'msg_err': "As senhas não podem ser iguais"})
        
        user = authenticate(username=usuario, password=oldPassword)
        
        if user is not None:
            user.set_password(newPassword)
            user.save()
            
            request.session['0'] = "A senha foi redefinida com sucesso."
            
            return redirect('/sicaweb/accounts/login/')
        else:
            return render(request, 'password.html', {'msg_err': "Usuário ou senha inválidos"})
    else:
        return render(request, 'password.html')

def login(request):
    if request.user.is_active:
        return home(request)
    else:
        return render(request, 'index.html')

def sica_login(request):
    username = request.POST['form-username']
    password = request.POST['form-password']
    user = authenticate(username=username, password=password)
    
    msg = []
    
    if user is not None:
        if user.is_active:
            auth_login(request, user)
            return redirect('/home')
        else:
            msg.append("login inativo")
    else:
        msg.append("Usuário ou senha inválidos")
    
    return render(request, 'index.html', {'errors': msg})
    
def sica_logout(request):
    logout(request)
    return redirect('/sicaweb')

def home(request):
    data = datetime.datetime.now().date()
    lstData = []  
    lstCardapio = []
    logado =  False
    
    if request.user.is_active:
        logado =  True
        
    for x in range(0,4):
        if(x == 0):
            qst = Cardapio.objects.filter(data=data)
        else:
            data = data+datetime.timedelta(days=1)
            
            if data.weekday() == 5:
                data = data+datetime.timedelta(days=2)
            
            qst = Cardapio.objects.filter(data=data)
       
        lstData.append(data)
        
        if qst:
            for i in qst:
                cardapio = i
            lstCardapio.append(cardapio)
        else:
            lstCardapio.append(qst)
        
    if request.method == 'POST':
        csrfContext = RequestContext(request)
        prontuario = request.POST.get("prontuario")
        senha = request.POST.get("senha")
        data = datetime.datetime.now()
        dMin = data.replace(hour=6, minute=40)
        dMax = data.replace(hour=9, minute=30)
        
        if u'PE' in prontuario.upper() and len(prontuario) == 9:
            prontuario = prontuario[2:]
            
        if data >= dMin and data <= dMax:
            data = data.date()
            aluno = Aluno.objects.filter(prontuario__icontains=prontuario, ativo=True)
            
            if aluno.count() == 1:
                aluno = aluno.first()
                horario = Horario.objects.filter(aluno__id__icontains=aluno.id)
                horario =  horario.first()
                ticket = Ticket.objects.filter(aluno__id__icontains=aluno.id, data__icontains=data)
    
                if ticket.count() == 0:
                            
                    if data.weekday() == 0:
                        valor = horario.seg;
                    elif data.weekday() == 1:
                        valor = horario.ter;
                    elif data.weekday() == 2:
                        valor = horario.qua;
                    elif data.weekday() == 3:
                        valor = horario.qui;
                    elif data.weekday() == 4:
                        valor = horario.sex;        
                    else:
                        erro = "Final de Semana"
                        return render(request, 'home.html', {'cardapio': lstCardapio, 'data':lstData, 'erro': erro, 'logado': logado}, csrfContext)  
                    
                    if not valor:
                        erro = "Dia inválido - Aluno não liberado"
                        return render(request, 'home.html', {'cardapio': lstCardapio, 'data':lstData, 'erro': erro, 'logado': logado}, csrfContext)  
                    
                    if senha == None and aluno.senha != u"":
                        return render(request, 'home.html', {'cardapio': lstCardapio, 'data':lstData, 'posProntuario': True, 'aluno': aluno, 'logado': logado}, csrfContext)
                    elif aluno.senha != u"":
                        if senha != aluno.senha:
                            return render(request, 'home.html', {'cardapio': lstCardapio, 'data':lstData, 'erro': "Senha Incorreta", 'logado': logado}, csrfContext)
                    
                    ticket = Ticket(
                        aluno = aluno,
                        data = data,
                        tipo = 1,
                    )
                    
                    ticket.save()
                     
                    return render(request, 'home.html', {'cardapio': lstCardapio, 'data':lstData, 'msg': 'ok', 'logado': logado}, csrfContext)
                else:
                    erro = "Gerado anteriormente"
                    return render(request, 'home.html', {'cardapio': lstCardapio, 'data':lstData, 'erro': erro, 'logado': logado}, csrfContext)    
            else:
                erro = "Prontuário Inválido"
                return render(request, 'home.html', {'cardapio': lstCardapio, 'data':lstData, 'erro': erro, 'logado': logado}, csrfContext)
        else:
            erro = "Fora do Horário para emissão de Ticket"
            return render(request, 'home.html', {'cardapio': lstCardapio, 'data':lstData, 'erro': erro, 'logado': logado}, csrfContext)
    else:
        return render(request, 'home.html', {'cardapio': lstCardapio, 'data':lstData, 'logado': logado})

def formata_data(s):
    DATE_FORMATS = ["%d/%m/%Y","%Y-%m-%d"]
    
    for date_format in DATE_FORMATS:
        try:
            return datetime.datetime.strptime(s, date_format)
        except ValueError:
            pass
        else:
            break
        
    return "Insira um valor válido"

@login_required(login_url='/accounts/login/')  
@permission_required('core.add_cardapio', raise_exception=True)
def cardapio(request):
    logado =  False
    
    if request.user.is_active:
        logado =  True
            
    if request.method == "POST":
        s = request.POST.get("campo")
        s = s.encode('utf8')
        
        campo = formata_data(s)
        
        if type(campo) == datetime.datetime:
            cardapios = Cardapio.objects.filter(data=campo)                                          
            csrfContext = RequestContext(request)
            if cardapios:
                return render(request, 'cardapio/cardapios.html', {'cardapios': cardapios, 'logado': logado}, csrfContext)
            else:
                cardapios = Cardapio.objects.all()
                cardapios = paginador(request, cardapios)
                msg = "Nenhum registro encontrado"
                return render(request, 'cardapio/cardapios.html', {'cardapios': cardapios, 'msg_err': msg, 'logado': logado})
        else:
            cardapios = Cardapio.objects.all()
            cardapios = paginador(request, cardapios)
            
            return render(request, 'cardapio/cardapios.html', {'cardapios': cardapios, 'msg_err': campo, 'logado': logado})
    else:
        cardapios = Cardapio.objects.all()
        cardapios = paginador(request, cardapios)
        
        if '0' in request.session:
            msg = request.session['0']
            del request.session['0']
            return render(request, 'cardapio/cardapios.html', {'cardapios': cardapios, 'msg': msg, 'logado': logado})
        
        return render(request, 'cardapio/cardapios.html', {'cardapios': cardapios, 'logado': logado})

@login_required(login_url='/accounts/login/')
@permission_required('core.add_cardapio', raise_exception=True)
def addCardapio(request):
    logado =  False
    
    if request.user.is_active:
        logado =  True
        
    if request.method == "POST":
        form = CardapioForm(request.POST)
        if form.is_valid():
            s = request.POST.get("data")
            s = s.encode('utf8')
            
            data = formata_data(s)           
            
            cardapio = Cardapio(
                data = data,
                principal = request.POST.get("principal"),
                salada = request.POST.get("salada"),
                sobremesa = request.POST.get("sobremesa"),
                suco = request.POST.get("suco"),
            )
            
            cardapio.save()
            
            request.session[0] = "Cardápio inserido com sucesso"
            return redirect('/sicaweb/cardapio')
        else:
            return render(request, 'cardapio/add_cardapio.html', {'form': form, 'logado': logado})
    else:
        form = CardapioForm
        return render(request, 'cardapio/add_cardapio.html', {'form': form, 'logado': logado})

@login_required(login_url='/accounts/login/')
@permission_required('core.change_cardapio', raise_exception=True)
def editarCardapio(request, id):
    logado =  False
    
    if request.user.is_active:
        logado =  True
        
    cardapio = get_object_or_404(Cardapio, pk = id)
    
    if request.method == 'POST':
        form = EditarCardapioForm(request.POST, initial={'id': id})
        if form.is_valid():
            s = request.POST.get("data")
            s = s.encode('utf8')
            
            data = formata_data(s)  
            
            cardapio.data = data
            cardapio.principal = request.POST.get("principal")
            cardapio.salada  = request.POST.get("salada")
            cardapio.sobremesa = request.POST.get("sobremesa")   
            cardapio.suco = request.POST.get("suco")
        
            cardapio.save()
        
            cardapio = Cardapio.objects.all()
            cardapio = paginador(request, cardapio)
            
            request.session[0] = "Cardapio alterado com sucesso"
            return redirect('/cardapio')
        else:
            return render(request, 'cardapio/editar.html', {'form': form, 'logado': logado})
    else:
        form = EditarCardapioForm(initial={
            'data': cardapio.data,
            'principal': cardapio.principal,
            'salada': cardapio.salada,
            'sobremesa': cardapio.sobremesa,
            'suco': cardapio.suco
            })
   
        return render(request, 'cardapio/editar.html', {'form': form, 'logado': logado})

@login_required(login_url='/accounts/login/')
@permission_required('core.delete_cardapio', raise_exception=True)
def deletarCardapio_confirm(request, id):
    qst = Cardapio.objects.filter(id=id)
    logado =  False
    
    if request.user.is_active:
        logado =  True
        
    for i in qst:
        cardapio = i
    return render_to_response('delete.html', {'cardapio': cardapio, 'app': 'Cardapio', 'logado': logado})

@login_required(login_url='/accounts/login/')
@permission_required('core.delete_cardapio', raise_exception=True)
def deletarCardapio(request, id):
    obj = Cardapio.objects.filter(id=id)
    obj.delete()
    request.session[0] = "Cardapio deletado com sucesso"
    return redirect('/cardapio')
