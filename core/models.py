# -*- coding: utf-8 -*-

from django.db import models
from django.db.models.fields import CharField

class Cardapio(models.Model):
    principal = models.TextField(max_length=255)
    salada = CharField(max_length=100)
    sobremesa = CharField(max_length=50)
    suco = CharField(max_length=50, null=True, blank=True)
    data = models.DateField()