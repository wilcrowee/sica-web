# SICA WEB

Para implementação do SICA são necessárias a seguinte infra:

1 Servidor web com python 2.x e postgres.

2 Computadores para terminal (Um para geração do ticket e outro para consumir o ticket).

2 Leitores de código de barras, para ler as cartinhas dos alunos.

Quanto ao funcionamento do sistema, existe a interface web para consultas, relatórios e emissão de ticket eletrônico, e dois terminais desktop que são executados um na CSP para emissão de ticket eletrônico e um na cantina para consumir o ticket gerado. Dessa forma o aluno pode gerar o ticket via WEB ou Terminal dentro do horário estipulado pela CSP junto a Empresa de Refeições, mas o consumo do mesmo só poderá ser feito na cantina através da leitura do código de barras da carteirinha ou da entrada manual do prontuário do aluno.

Recentemente inserimos uma opção de senha para que o ticket gerado por um aluno não seja utilizado por outro, esta senha é cadastrada pela CSP.

Atualmente estamos disponibilizando o sistema pelo GIT:

Sistema WEB:
https://gitlab.com/wilcrowee/sica-web

Terminal de Emissão de Ticket:
https://gitlab.com/wilcrowee/Desktop-II

Terminal de Consumo de Ticket:
https://gitlab.com/wilcrowee/SICA


Os Terminais foram desenvolvidos em java e os projetos devem ser abertos no Eclipse e compilados, o sistema web foi desenvolvido usando Python 2.7 e Django e banco de dados Postgres. Para se usar o sistema web deve-se criar um bd no postgres alterar as configurações referentes ao banco e rede de acesso dentro da pasta sica/settings.py.

Depois deste processo basta executar o comando migrate e toda a estrutura do banco de dados será criada.
