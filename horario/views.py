# -*- coding: utf-8 -*-

from django.shortcuts import render, render_to_response, get_object_or_404, redirect
from django.template.context import RequestContext
from django.contrib.auth.decorators import login_required, permission_required

from horario.forms import HorarioForm, HorarioCursoForm, EditHorarioForm
from horario.models import Horario
from curso.views import Curso
from aluno.views import paginador
from aluno.models import Aluno
from itertools import chain

@login_required(login_url='/sicaweb/accounts/login/')
@permission_required('horario.add_horario', raise_exception=True)
def horario(request):
    horarios = Horario.objects.all()
    
    if request.method == 'POST':
        campo = request.POST.get('campo')
        
        nome = Horario.objects.filter(aluno__nome__icontains = campo)
        sobrenome = Horario.objects.filter(aluno__sobrenome__icontains = campo)
        
        search = list(chain(nome, sobrenome))
        search = list(set(search))
        
        csrfContext = RequestContext(request)
        
        if search:
            return render(request, 'horario/horarios.html', {'horarios': search, 'registros': search.__len__()}, csrfContext)
        else:
            err = "Nenhum registro encontrado"
            horarios = paginador(request, horarios)
            return render(request, 'horario/horarios.html', {'horarios': horarios, 'err': err}, csrfContext)

    else:
        horarios = paginador(request, horarios)
        
        if '0' in request.session:
            msg = request.session['0']
            del request.session['0']
            return render(request, 'horario/horarios.html', {'horarios': horarios, 'msg': msg})
            
        return render(request, 'horario/horarios.html', {'horarios': horarios})

def validaDia(dia):
    if dia:
        return True 
    
    return False

@login_required(login_url='/sicaweb/accounts/login/')
@permission_required('horario.add_horario', raise_exception=True)
def addHorario(request):
    cursos = Curso.objects.filter(ativo=True)
    
    if request.method == "POST":
        if request.POST.get('tipo') == '1':
            form = HorarioForm(request.POST)
            if form.is_valid():            
                id =  request.POST.get("aluno")
                
                aluno = get_object_or_404(Aluno, pk = id)
              
                horario = Horario(
                    aluno = aluno,
                    seg = validaDia(request.POST.get("seg")),
                    ter = validaDia(request.POST.get("ter")),
                    qua = validaDia(request.POST.get("qua")),
                    qui = validaDia(request.POST.get("qui")),
                    sex = validaDia(request.POST.get("sex")),
                )
                
                horario.save()
                
                horarios = Horario.objects.all()
                horarios = paginador(request, horarios)
                
                request.session[0] = "Horario inserido com sucesso"
                return redirect('/sicaweb/horarios')
            else:
                return render(request, '/horario/add_horario.html', {'form': form, 'cursos': cursos})
        else:
            formcurso = HorarioCursoForm(request.POST)
            if formcurso.is_valid():
                curso = request.POST.get('curso')
                ano =  request.POST.get('ano')
                
                alunos =  Aluno.objects.filter(curso=curso).filter(ano=ano)
                
                for aluno in alunos:
                    horario = Horario.objects.filter(aluno=aluno)
                    
                    if horario:
                        horario = horario[0]
                        
                        horario.aluno = aluno
                        horario.seg = validaDia(request.POST.get("seg"))
                        horario.ter = validaDia(request.POST.get("ter"))
                        horario.qua = validaDia(request.POST.get("qua"))
                        horario.qui = validaDia(request.POST.get("qui"))
                        horario.sex = validaDia(request.POST.get("sex"))    
                    else:
                        horario = Horario(
                            aluno = aluno,
                            seg = validaDia(request.POST.get("seg")),
                            ter = validaDia(request.POST.get("ter")),
                            qua = validaDia(request.POST.get("qua")),
                            qui = validaDia(request.POST.get("qui")),
                            sex = validaDia(request.POST.get("sex")),
                        )
                    
                    horario.save()
                
                horarios = Horario.objects.all()
                horarios = paginador(request, horarios)
                request.session[0] = "Horario inserido com sucesso"
                return redirect('/sicaweb/horarios')
            else:
                form = HorarioForm
                return render(request, 'horario/add_horario.html', {'form': form, 'formcurso': formcurso, 'cursos': cursos, 'id': 1})
                
    else:
        form = HorarioForm
        return render(request, 'horario/add_horario.html', {'form': form, 'cursos': cursos})

@login_required(login_url='/sicaweb/accounts/login/')
@permission_required('horario.change_horario', raise_exception=True)
def editarHorario(request, id):
    horario = get_object_or_404(Horario, pk=id)
    
    if request.method == "POST":
        form = EditHorarioForm(request.POST, initial={'id': id}) 
        
        if form.is_valid():
            horario.aluno = get_object_or_404(Aluno, pk=request.POST.get('aluno'))
            horario.seg = validaDia(request.POST.get('seg'))
            horario.ter = validaDia(request.POST.get('ter'))
            horario.qua = validaDia(request.POST.get('qua'))
            horario.qui = validaDia(request.POST.get('qui'))
            horario.sex = validaDia(request.POST.get('sex'))
            
            horario.save()
                        
            request.session[0] = "Horario alterado com sucesso"
            return redirect('/sicaweb/horarios')
        else:
            return render(request, 'horario/editar.html', {'form': form})
    else:
        form = HorarioForm(initial={
            'aluno': horario.aluno,
            'seg': horario.seg,
            'ter': horario.ter, 
            'qua': horario.qua,
            'qui': horario.qui,
            'sex': horario.sex,
        })
    
        return render(request, 'horario/editar.html', {'form': form})

@login_required(login_url='/sicaweb/accounts/login/')
@permission_required('horario.delete_horario', raise_exception=True)
def deletarHorario_confirm(request, id):
    qst = Horario.objects.filter(id=id)
    for i in qst:
        horario = i
    return render_to_response('delete.html', {'horario': horario, 'app': 'Horario'})

@login_required(login_url='/sicaweb/accounts/login/')
@permission_required('horario.delete_horario', raise_exception=True)
def deletarHorario(request, id):
    obj = get_object_or_404(Horario, pk = id)
    obj.delete()

    request.session[0] = "Horário deletado com sucesso"
    return redirect('/sicaweb/horarios')
