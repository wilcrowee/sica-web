# -*- coding: utf-8 -*-

from django.contrib import admin
from horario.models import Horario

class HorarioAdmin(admin.ModelAdmin):
    list_display = ('id','aluno','seg','ter','qua','qui','sex')
    ordering = ('id','aluno','seg','ter','qua','qui','sex')
    search_fields = ('id','aluno','seg','ter','qua','qui','sex')
    list_display_icons = True
    export_to_xls = True
    
admin.site.register(Horario, HorarioAdmin)