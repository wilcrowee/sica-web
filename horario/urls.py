# -*- coding: utf-8 -*-

from django.conf.urls import url
from horario import  views

urlpatterns = [
    url(r'^$', views.horario),
    url(r'^add', views.addHorario),
    url(r'^edit/(?P<id>[0-9]+)/$', views.editarHorario, name='editar_horario'),
    url(r'^delete/(?P<id>[0-9]+)/$', views.deletarHorario_confirm, name='deletar_horario'),
    url(r'^delete/pos_confirm/(?P<id>[0-9]+)/$', views.deletarHorario, name='deletar_horario_posconfirm'),  
]