# -*- coding: utf-8 -*-

from django import forms
from horario.models import Horario
from aluno.models import Aluno

class HorarioForm(forms.ModelForm):

    class Meta:
        model = Horario
        fields = ('aluno','seg','ter','qua','qui','sex')
    
    def __init__(self, *args, **kwargs):
        super(HorarioForm, self).__init__(*args, **kwargs)
        self.fields['aluno'].queryset = Aluno.objects.filter(ativo=True).order_by("nome")
        self.fields['aluno'].label_from_instance = lambda obj: "%s %s" % (obj.nome, obj.sobrenome)
    
    def clean(self):
        super(HorarioForm, self).clean()
        aluno = self.cleaned_data.get('aluno')
        
        aluno = Horario.objects.filter(aluno=aluno)
        
        if self.errors:
            return self.cleaned_data
        
        if aluno:
            self.add_error('aluno', u'Aluno cadastrado anteriormente')
            
        return self.cleaned_data

class HorarioCursoForm(forms.ModelForm):
    curso = forms.IntegerField()
    ano = forms.IntegerField()        

    class Meta:
        model = Horario
        fields = ('curso','ano','seg','ter','qua','qui','sex')
    
    def clean(self):
        super(HorarioCursoForm, self).clean()
        curso = self.cleaned_data.get('curso')
        ano = self.cleaned_data.get('ano')
        
        if int(ano) == 0:
            self.add_error('ano', u'Um ano deve ser selecionado')
            
        if int(curso) == 0:
            self.add_error('curso', u'Um curso válido deve ser selecionado')
        
        return self.cleaned_data
    
class EditHorarioForm(forms.ModelForm):
    
    class Meta:
        model = Horario
        fields = ('aluno','seg','ter','qua','qui','sex')
        
    def __init__(self, *args, **kwargs):
        super(EditHorarioForm, self).__init__(*args, **kwargs)
        self.fields['aluno'].queryset = Aluno.objects.all().order_by("nome")
        self.fields['aluno'].label_from_instance = lambda obj: "%s %s" % (obj.nome, obj.sobrenome)
    
    
    def clean(self):
        super(EditHorarioForm, self).clean()
        aluno = self.cleaned_data.get('aluno')
        id = self.initial.get('id')
        
        horario_new = Horario.objects.filter(aluno=aluno)
        horario_old = Horario.objects.filter(id=id)
        
        if horario_new:
            horario_new = horario_new[0]
            if horario_old:
                horario_old = horario_old[0]
                if horario_new.aluno != horario_old.aluno:
                    self.add_error('aluno', u'O Aluno já tem um horario registrado')
        
        return self.cleaned_data
