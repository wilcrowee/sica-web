from __future__ import unicode_literals

from django.db import models
from aluno.models import Aluno

class Horario(models.Model):
    aluno = models.ForeignKey(Aluno)
    seg = models.BooleanField()
    ter = models.BooleanField() 
    qua = models.BooleanField()
    qui = models.BooleanField()
    sex = models.BooleanField()