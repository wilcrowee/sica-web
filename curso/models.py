# -*- coding: utf-8 -*-

from django.db import models 
from django.db.models.fields import CharField

class Curso(models.Model):
    curso = CharField(max_length=255)
    ativo = models.BooleanField()


class Search(models.Model):
    campo = CharField(max_length=255) 
