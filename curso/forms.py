# -*- coding: utf-8 -*-
from django import forms
from curso.models import Curso

class CursoForm(forms.ModelForm):

    class Meta:
        model = Curso
        fields = ('curso','ativo',)
    
    def clean(self):
        super(CursoForm, self).clean()
        
        curso = self.cleaned_data.get('curso')
        cursos = Curso.objects.all()

        if self.errors:
            return self.cleaned_data
        
        for item in cursos:
            if item.curso.lower() == curso.lower():
                self.add_error('curso', u'Curso já cadastrado')
            
        return self.cleaned_data
    
class EditarCursoForm(forms.ModelForm):
    __ID = 0
    
    def __init__(self, *args, **kwargs):
        self.__ID = kwargs.pop('curso_id')
        super(EditarCursoForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Curso
        fields = ('curso','ativo')
    
    def clean(self):
        super(EditarCursoForm, self).clean()
        
        curso_id = int(self.__ID)
        curso = self.cleaned_data.get('curso')
        cursos = Curso.objects.all()
              
        if self.errors:
            return self.cleaned_data
        
        for item in cursos:
            if item.curso.lower() == curso.lower():
                if item.id != curso_id:
                    self.add_error('curso', u'Curso já cadastrado')
        
        return self.cleaned_data
